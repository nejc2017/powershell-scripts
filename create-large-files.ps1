# Create large files with PowerShell
# https://medium.com/@developer82/windows-powershell-creating-large-files-on-disk-fast-306d8df9d116

function CreateFile {
    param (
        [string]$FileSize,
        [string]$FileCount
    )
 
    for ($i = 0; $i -lt $FileCount; $i++) {
        <# Action that will repeat until the condition is met #>
        $FileSize = ($SizeInMB -as [int]) * 1024 * 1024 #convert to kb
        $OutputFileName = (Get-Date -Format "yyyMMdd-ffff") + "-GeneratedFile"
        
        $File = [System.IO.File]::Create($OutputFileName)
        $File.SetLength($FileSize)
        $File.Close()

        Write-Information -MessageData "Created File: $OutputFileName and Size: $SizeInMB"  -InformationAction Continue 

    }
    Write-Information -MessageData "Files are stored in $(Get-Location)"  -InformationAction Continue 
}


$SizeInMB = Read-Host -Prompt "How many MB should the file have?"
[int]$FileCount = Read-Host -Prompt "Number of files?"

CreateFile $SizeInMB $FileCount
