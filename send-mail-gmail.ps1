﻿# Send GMail email
# Turn on "less secure apps" or "app password"
$sender = "test1@gmail.com"
$receiver ="test2@gmail.com"
$subject =""
$body =""

$smtpServer="smtp.gmail.com"
$SMTPPort = "587"

if ($subject -eq $null){
    $subject = Read-Host -Prompt "Please enter the Subject:"
    $body = Read-Host -Prompt "Please enter your message:"
}

Send-MailMessage -From $sender -To $receiver -Subject $subject -Body $body -SmtpServer $smtpServer -Port  $SMTPPort -UseSsl -Credential (Get-Credential)