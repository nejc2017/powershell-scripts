﻿
# check if administrator
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
[Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Host "Not running as Admin. Please restart script with admin privileges"
    break
}
else {
    Write-Host "Running as admin"
}

$serviceName = Read-host -Prompt "Which service to stop?"
[array] $foundServices  = Get-Service $serviceName

# check all results
foreach ($service in $foundServices) {
    # iterate through results and check the running services
    if ($service.Status -eq "running") {
        Write-Host "Found: " $Service.Name $Service.Status

        do {
            # ask for permission to stop them and display status
            $stopServicePrompt = Read-Host -Prompt "Stop service Y/N ?"
            $stopServicePrompt = $stopServicePrompt.ToUpper()
            if ($stopServicePrompt -eq "Y") {
                Stop-Service $service.Name
                Write-Host "Service is stopped " $service.Name
                Get-service $service | Write-Host $_.Status
            }
        } until ($stopServicePrompt -eq "Y" -or "N")
    }
    else {
        Write-Host $service $service.Status
    }
    Write-Host "..."
}

Read-Host -Prompt "Press any key to close"