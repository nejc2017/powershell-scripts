# Powershell website scraper and attachment downloader
# https://www.altaro.com/msp-dojo/web-scraping-tool-for-msps/
# https://4sysops.com/archives/use-powershell-to-download-a-file-with-http-https-and-ftp/
# https://stackoverflow.com/questions/56809063/how-to-parse-the-html-of-a-website-with-powershell

$websiteUrl ="http://ringerajarec.nsf-music.com/releases.html"
$downloadPath ="c:\admin\scrape"
$sleepTime = 5

$data = Invoke-webrequest -Uri $websiteUrl

# # search inside a html table element
# # $websiteTable = $data.ParsedHTML.getElementsByTagName("table") 
# # $websiteTable > "$downloadPath\output.txt"

# Test: display all data
#Write-host $data

$links = $data.links | select innertext, href 

# test: display all links
# Write-yHost $links

# filter by extension / check href
# example with multiple filters: $filteredLinks = $links | where {($_.href -like "*mod") -or ($_.href -like "*it") -or ($_.href -like "*xm") } 
$filteredLinks = $links | where {($_.href -like "*flac")} 

# display selection
$filteredLinks.href

Write-Host "`r`nDownload? (y/n)"
$inputText = Read-Host

    if ($inputText -eq "y") {
        
        # if the folder does not exist, create it
        if (!(Test-Path -Path $downloadPath)) {
            New-Item -ItemType dir $downloadPath
        }

    # iterate, define filename and download
    foreach ($lnk in $filteredLinks.href) {
        $lastindex = $lnk.LastIndexOf("/");
        
        #
        # split the string at the '#' char - example https://api.filteredLinksarchive.org/downloads.php?filteredLinksuleid=32331#_angeloc.xm
        #
        # $split = $lnk -split "#"
        # remove the last char
        # $filename = $split[1].Substring(0,$split[1].Length-1)

        #
        # split the string at the last '/' char -  example http://www.archive.org/download/RRR001/RRR001-A_Fexomat_and_Sickhead_-_Juriiii.flac
        #
        $filename = $lnk.Substring($lastindex+1);
        Write-host "downloading $downloadPath\$filename."

        Invoke-WebRequest -Uri $lnk -Out "$downloadPath\$filename"
        # chill for a bit
        Start-Sleep -Seconds $sleepTime

    }
}
else{
    Write-Host "Aborted."
}