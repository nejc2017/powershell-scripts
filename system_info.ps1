﻿# Get-ComputerInfo


Get-WmiObject Win32_BaseBoard

Get-WmiObject  Win32_physicalmemory | Format-Table Manufacturer, ConfiguredClockSpeed, Capacity, Banklabel, Serialnumber, Partnumber

Get-CIMInstance Win32_OperatingSystem
Get-CIMInstance Win32_OperatingSystem | Select FreePhysicalMemory, TotalVisibleMemorySize

Write-Host "-----"
Write-Host "Memory usage of processes"
Write-Host "-----"

Get-Process | Sort-Object -Property WS  -Descending | Select-Object -first 15 | Format-Table Name, WS

Get-CimInstance Win32_LogicalDisk