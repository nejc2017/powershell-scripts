# Get size of file (header) and convert it to 1MB

$url = "https://archive.org/download/RRR042/RRR042-AA__Oblin_-_Gas_Man.flac"

$fileSize = (Invoke-WebRequest $url -Method Head).Headers.'Content-Length'/1MB
$fileSize