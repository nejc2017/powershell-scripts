<#
# Get expiration date of certificate
# https://www.tutorialspoint.com/how-to-get-website-ssl-certificate-validity-dates-with-powershell

$req = $null

$url = "http://google.com"
# ignore the certificate check  (if http)
[Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

$req = [Net.HttpWebRequest]::Create($url)
$req.GetResponse() | Out-Null

$req.ServicePoint.Certificate.GetEffectiveDateString()

#>


# Hostmonitor variables
$statusAlive       = "ScriptRes:Host is alive:"
$statusDead        = "ScriptRes:No answer:"
$statusUnknown     = "ScriptRes:Unknown:"
$statusNotResolved = "ScriptRes:Unknown host:"
$statusOk          = "ScriptRes:Ok:"
$statusBad         = "ScriptRes:Bad:"
$statusBadContents = "ScriptRes:Bad contents:"

# https://packetlost.com/blog/2017/02/04/check-the-date-of-a-certificate-from-a-polled-url-with-powershell/


#url for the webpae is the first parameter from Hostmonitor
$websiteUrl = $args[0]
$minimumCertAgeDays = 7
$expirationDate = $null
$daysLeft = $null



try { 
    $webcall = Invoke-WebRequest $websiteUrl
    $servicePoint = [System.Net.ServicePointManager]::FindServicePoint($websiteUrl)
    # $servicePoint.Certificate.GetCertHashString()
    $expirationDate = $servicePoint.Certificate.GetExpirationDateString()

    $daysLeft = New-timespan -Start (Get-Date) -End $expirationDate
    $daysLeft = $daysLeft.Days

    if ( $daysLeft -gt $minimumCertAgeDays)
    {
        echo $statusOk$daysLeft
        # Write-Host $expirationDate
        # Write-Host $daysLeft.Days

    }
    else
    {
        echo $statusBad$daysLeft
        # Write-Host $expirationDate
        # Write-Host $daysLeft.Days
    }

}
catch 
{
    echo $statusUnknown
}


