$confirmation = Read-Host "Nadaljujem?"

if ($confirmation -ne 'y') {

#  Get a list of services and order them by startup type
 
Get-Service | select Name,Status,StartType,StartupType | Sort-Object StartType | out-gridview

# Export to txt
Get-Service | select Name,Status,StartType,StartupType | Sort-Object StartType | out-file -Encoding utf8 c:\admin\services$(Get-Date -f yyyy-MM-dd-HHmmss).txt

# Export to CSV
Get-Service | select DisplayName,Status,StartType,StartupType | Sort-Object StartType | Export-CSV c:\admin\services$(Get-Date -f yyyy-MM-dd-HHmmss).txt

# Import CSV and compare with current running services
$currentServices = $prevServices = $null
$currentServices =  Get-Service | select DisplayName,Status,StartType,StartupType | Sort-Object StartType
$prevServices = Import-CSV "C:\admin\services2022-01-22-204035.txt"
Compare-Object $currentServices $prevServices -Property DisplayName,Status

# Start Exchange services
Get-Service *Exchange* | Where {($_.StartType -eq 'Automatic') -and ($_.Status -ne 'Running')} | Start-Service



# HKLM
# https://devblogs.microsoft.com/scripting/exclude-delayed-start-services-when-checking-status-with-powershell/

# Rest exchange
# https://www.alitajran.com/restart-exchange-server-services-through-powershell/#h-restart-all-exchange-services-with-startup-type-automatic

}