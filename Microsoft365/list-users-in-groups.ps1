<#
    List users in groups
    and groups in groups
#>
# TODO show users inside of groups

# Recursion
# Link: https://mcpmag.com/articles/2017/01/05/working-with-recursive-functions.aspx?m=1
function Get-AllGroupMembers {
    [CmdletBinding()]
    param (
        [Parameter()]
        [string]$GroupID 
    )
    $GroupMembers = Get-AzureADGroupMember -ObjectId $GroupID -All $True
    foreach ($Member in $GroupMembers) {
        
        if ($Member.ObjectType -eq 'Group') {
            Get-AllGroupMembers $Member.ObjectId
            Write-Host "--Happy - Recursion!--"
        }
        elseif ($Member.ObjectType -eq 'User') {
            # this is a user not a group
            $User = [pscustomobject]@{
                'GroupName' = $Null
                'GroupId' = $GroupID
                'ObjectId' = $Member.ObjectId
                'DisplayName' = $Member.DisplayName
                'UPN' = $Member.UserPrincipalName
                'UserType' = $member.UserType
            }
            $User
        }
    }
}

# Check if the module is installed
if (-not(Get-Module -ListAvailable -Name AzureAD)) {
    Install-Module -Name AzureAD -Scope Local
}
Import-Module AzureAD

#TODO remove tenantID for Lotric unLTD test tenant
# $TenantID = 'd22b8946-bc06-4ba3-b774-3c9fecf2f5dc'

# Connect to AzureAD
# Connect-AzureAD 

# List all groups in Out-Gridview
$AzureADGroupsWithNames = Get-AzureADMSGroup -All $True
$AzureADGroupsWithNames | select * | out-gridview

$ListedGroups =  New-Object System.Collections.Generic.List[PSCustomObject]
foreach ($Group in $AzureADGroupsWithNames) { 
    # Call the recursion function
    # $UsersInGroup = Get-AllGroupMembers $Group.Id
    $ListedGroups.Add($(Get-AllGroupMembers $Group.Id))
 

    # foreach ($SingleUser in $UsersInGroup) {
    #     $ListedGroups.Add($SingleUser)
    # }
}

# Search and match Group names from the new $ListedGroups collection to $AzureADGroupsWithNames.IDs
foreach ($GroupEntry in $ListedGroups) {
    foreach ($AdNamedGroup in $AzureADGroupsWithNames) {
        if ($GroupEntry.GroupId -eq $AdNamedGroup.Id) {
            $GroupEntry.GroupName = $AdNamedGroup.DisplayName
        }
    }
}

# Display group objects inside groups
$ListedGroups | Sort-Object GroupId | Out-GridView

