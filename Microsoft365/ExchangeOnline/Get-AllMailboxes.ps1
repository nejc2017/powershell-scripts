<#
    Get all Exchange Online mailboxes and detailed statistics about them
### #>

if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-EXOMailbox -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline

}

try {
    # Using the v2 cmdlet
    $AllMailboxes = Get-EXOMailbox -ResultSize unlimited -PropertySets Minimum, Archive, Quota

    $UserSelection = Read-Host "Display Mailbox report in Out-Gridview(1) or export as CSV(2)"
    if ($UserSelection.ToLower() -eq '1') {
        Write-Information 'Showing Out-GridView' -InformationAction Continue
        $AllMailboxes | out-gridview -Title 'Mailboxes report: Property sets: Minimum, Archive, Quota'
    }
    elseif ($UserSelection.ToLower() -eq '2') {
        # Issues with Get-Location when debugging in VS Code / ISE
        # Solution: https://stackoverflow.com/a/62604759
        
        $Directorypath = if ($PSScriptRoot) { $PSScriptRoot } `
        elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
        elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }
    
        $TimeStamp = Get-Date -Format "yyyy-MM-dd-ffff"
        try {
            $AllMailboxes | Export-Csv -Delimiter ';' -Encoding UTF8 -Path "$Directorypath\$TimeStamp-ExchangeMailboxesExport.csv"
            Write-Information "$Directorypath\$TimeStamp-ExchangeMailboxesExport.csv" -InformationAction Continue
            
        }
        catch {
            Write-Warning $Error[0]
        }
    }
    else {
        Write-Information 'Please enter 1 or 2. Exiting...'
    }
}
catch {
    Write-Warning $Error[0]
}
finally {
    Disconnect-ExchangeOnline
}



