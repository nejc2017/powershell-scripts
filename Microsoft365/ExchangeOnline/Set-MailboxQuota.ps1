# Set Mailbox Quota in GB
###

if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-Mailbox -ErrorAction Stop | format-table
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline
}


$AllMailboxes =  Get-Mailbox
Write-Information 'Select a user mailbox' -InformationAction Continue
$SelectedMailbox = $AllMailboxes | select UserPrincipalName, DisplayName, RecipientType, IssueWarningQuota, ProhibitSendQuota, ProhibitSendReceiveQuota | Out-GridView -Title 'Select a user mailbox change the quota' -OutputMode Single
$SelectedMailbox | select UserPrincipalName, IssueWarningQuota, ProhibitSendQuota, ProhibitSendReceiveQuota | format-table

# Ask for quota input
$IssueWarning = Read-Host "Set Issue Warning Quota in GB"
$ProhibitSend = Read-Host "Set Prohibit Send Quota in GB"
$ProhibitSendReceive = Read-Host "Set Prohibit Send Receive Quota in GB"

try {
    Set-Mailbox -Identity $SelectedMailbox.UserPrincipalName -IssueWarningQuota "$IssueWarning GB" -ProhibitSendQuota "$ProhibitSend GB" -ProhibitSendReceiveQuota "$ProhibitSendReceive GB"
    
}
catch {
    Write-Warning $Error[0]
}
finally{
    Write-Information 'Showing the changes in user quota' -InformationAction continue
    Get-Mailbox $SelectedMailbox.UserPrincipalName | select UserPrincipalName, IssueWarningQuota, ProhibitSendQuota, ProhibitSendReceiveQuota | Format-Table
    Disconnect-ExchangeOnline
}