# Set Autoreply - set internal and external messages
###

if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-EXOMailbox -ErrorAction Stop | format-table
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline
}

# Show all users, allow to select one
$AllMailboxes =  Get-EXOMailbox
Write-Information 'Select a user mailbox' -InformationAction Continue
$SelectedMailbox = $AllMailboxes | Out-GridView -Title 'Select a user mailbox to add an autoreply' -OutputMode Single

#Messages
Write-Information 'Please enter the autoreply message. Add HTML if required' -InformationAction continue
$DirectoryPath = if ($PSScriptRoot) { $PSScriptRoot } `
elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }


$InternalPath = "$DirectoryPath\Autoreply-Internal.txt"
$ExternalPath = "$DirectoryPath\Autoreply-External.txt"


try {
    Write-Information 'Internal message: Opening notepad. Add HTML if required.' -InformationAction continue
    Start-Process notepad.exe $InternalPath -Wait
    $InternalMessage = Get-Content -Path $InternalPath
}
catch {
    Write-Warning $Error[0]
}

try {
    Write-Information 'External message: Opening notepad. Add HTML if required.' -InformationAction continue
    Start-Process notepad.exe $ExternalPath -Wait
    $ExternalMessage = Get-Content -Path $ExternalPath
    
}
catch {
    Write-Warning $Error[0]
}


try {
    Set-MailboxAutoReplyConfiguration -Identity $SelectedMailbox.UserPrincipalName -AutoReplyState Enabled -InternalMessage $InternalMessage -ExternalMessage $ExternalMessage
    
}
catch {
    Write-Warning $Error[0]
}
finally {
    Write-Information 'Showing Autoreply configuration' -InformationAction continue
    Get-MailboxAutoReplyConfiguration -Identity $SelectedMailbox.UserPrincipalName | Out-GridView
    Disconnect-ExchangeOnline
}

