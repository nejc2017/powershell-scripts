# Import-module ExchangeOnlineManagement
# Connect-ExchangeOnline 

$dateStart = (get-date).AddDays(-10)
$dateEnd = (get-date)
$searchTerm = '*nejc*'

$searchResults = New-Object Collections.Generic.List[pscustomobject]
$page = 1
$results = 0
do {
    $mailMessages = Get-MessageTrace -StartDate $dateStart -EndDate $dateEnd -PageSize 5000 -Page $page 
    $filteredMessages = $mailMessages | Where {($_.Subject -like $searchTerm) -Or ($_.SenderAddress -like $searchTerm) -Or ($_.RecipientAddress -like $searchTerm)}
    $searchResults.Add($filteredMessages)

    $results = $mailMessages.Count 
    Write-Information -InformationAction Continue "Page $i with $results results"
    $page++
}
while ($results -gt 0) 

Write-Information -InformationAction Continue "Opening Message Trace"
$selectedResults = $searchResults | Out-HtmlView -PassThru -Title "Message Trace $(Get-Date)"
Write-Information -InformationAction Continue "Opening Message Details"
$selectedResults | Get-MessageTraceDetail | Select * | Out-HtmlView -Title "Message Detail $(Get-Date)"
