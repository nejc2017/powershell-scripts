# Get specific mailbox folder statistics/Usage
###

if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-EXOMailbox -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline

}

$AllMailboxes = Get-EXOMailbox -ResultSize unlimited
$SelectedMailboxes = $AllMailboxes | Out-GridView -Title 'Select mailboxes for additional folder statistics' -PassThru


$UserSelection = Read-Host "Display Mailbox Report in Out-Gridview(1) or export as CSV(2)"
if ($UserSelection.ToLower() -eq '1') {

    foreach ($CurrentMailbox in $SelectedMailboxes) {
        $UPN = $CurrentMailbox.UserPrincipalName
        $MailboxDetails = $CurrentMailbox | Get-EXOMailboxFolderStatistics  -FolderScope All | Select-Object Name, ContentMailboxGuid, FolderType, FolderSize, ItemsInFolderAndSubFolders, DeletedItemsInFolderAndSubfolders, FolderAndSubfolderSize
        Write-Information 'Showing Out-GridView' -InformationAction Continue
        $MailboxDetails | Out-GridView -Title "Details about the Mailbox: $UPN"
    }

}
elseif ($UserSelection.ToLower() -eq '2') {
    # Issues with Get-Location when debugging in VS Code / ISE
    # Solution: https://stackoverflow.com/a/62604759
    
    $Directorypath = if ($PSScriptRoot) { $PSScriptRoot } `
    elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
    elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }

    $TimeStamp = Get-Date -Format "yyyy-MM-dd-ffff"
    try {
        $DetailedMailboxes = $SelectedMailboxes | Get-EXOMailboxFolderStatistics  -FolderScope All | Select-Object Name, ContentMailboxGuid, FolderType, FolderSize, ItemsInFolderAndSubFolders, DeletedItemsInFolderAndSubfolders, FolderAndSubfolderSize
        $DetailedMailboxes | Export-Csv -Delimiter ';' -Encoding UTF8 -Path "$Directorypath\$TimeStamp-ExchangeMbxFolderStastistics.csv"
        Write-Information "$Directorypath\$TimeStamp-ExchangeMbxFolderStastistics.csv" -InformationAction Continue
        
    }
    catch {
        Write-Warning $Error[0]
    }
}
else {
    Write-Information 'Please enter 1 or 2. Exiting...'
}