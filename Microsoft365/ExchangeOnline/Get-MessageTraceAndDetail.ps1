# Gets info about a specific message and displays additional details
# Combines Get-MessageTrace and Get-MessageDetail

if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-Mailbox -ErrorAction Stop | format-table
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline
}

# Select a mailbox
$ReceiverMailbox = Read-Host "Please enter the Receiver's e-mail address or leave it blank"
$SenderMailbox = Read-Host "Please enter the Sender's e-mail address or leave it blank"


# Select a date
# Message trace only allows to search from the past 10 days. Older traces require to use the cmdlet Start-HistoricalSearch
[DateTime] $DateNow = Get-Date
$DateRange = New-Object System.Collections.Generic.List[DateTime]
for ($i = 0; $i -lt 10; $i++) {
    $DateRange.Add($DateNow.AddDays(-$i))
}

# Display dates
Write-Information "Displaying available dates for Get-MailTrace. If the message is older, please select Historical Search" -InformationAction Continue
$Count = 1
foreach ($Dt in $DateRange) {
    Write-Information "$Count $($Dt.Date.ToString('dd.MM.yyyy'))" -InformationAction Continue
    $Count++
}

# Mail trace or Historical search selection
Write-Information "Is the Start Date older than the Dates listed?" -InformationAction Continue
do {
    $IsHistoricalInput = Read-Host "Please enter Y/N"
} while (($IsHistoricalInput.ToLower() -ne 'y') -and ($IsHistoricalInput.ToLower() -ne 'n'))

# Historical search
if ($IsHistoricalInput.ToLower() -eq 'y') {
    # Date is earlier than 10 days, using Historical Search
    $StartDateHistorical = Read-Host "Using Historical Search. Please enter the Start Date (as dd.MM.yyyy)"
    # Parse the date provided
    $StartDateHistoricalParsed = [Datetime]::ParseExact($StartDateHistorical, 'dd.MM.yyyy', $null)
    $EndDateHistorical = Read-Host "Please enter the End Date (as dd.MM.yyyy)"
    $EndtDateHistoricalParsed = [Datetime]::ParseExact($EndDateHistorical, 'dd.MM.yyyy', $null)
    $NotifyEmail = Read-Host "Please enter the email to be notified when the search is completed"

    $ReportTypeInput = Read-Host "Please select the Report Type: 1) Message Trace 2) Message Trace Detail 3) Phishing 4) Spam"
    $Report = 'MessageTrace'
    switch ($ReportTypeInput) {
        '1' {$Report = 'MessageTrace'}
        '2' {$Report = 'MessageTraceDetail'}
        '3' {$Report = 'Phish'}
        '4' {$Report = 'Spam'}
        Default {'Incorrect number, using Message Trace'
            $Report = 'MessageTrace'}
    }

    try {
        # Splatting https://devblogs.microsoft.com/scripting/use-splatting-to-simplify-your-powershell-scripts/
        $ReportTitle = Get-Date -Format 'yyyyMMdd-ffff'
        $TraceInfo = @{
            StartDate = $StartDateHistoricalParsed
            EndDate = $EndtDateHistoricalParsed
            ReportType = $Report
            ReportTitle = "$ReportTitle-Trace"
        }
        # Check if Sender/Receiver is empty, add property
        if($SenderMailbox){$Traceinfo.Add('SenderAddress', $SenderMailbox)}
        if($ReceiverMailbox){$Traceinfo.Add('RecipientAddress', $ReceiverMailbox)}
        if($NotifyEmail){$Traceinfo.Add('NotifyAddress', $NotifyEmail)}

        Write-Information "Requesting Historical Search ..." -InformationAction Continue
        Start-HistoricalSearch @TraceInfo
    }
    catch {
        Write-Warning -Message $Error[0]
    }
    finally {
        Get-HistoricalSearch
        Disconnect-ExchangeOnline
    }
}
# last 10 days message trace
else {
    # Date selection
    do {
        $StartDateNo = Read-Host "Please select the Start Date number"
        $EndDateNo = Read-Host "Please select the End Date number"

    } # should not be wower than or equal
    while (($StartDateNo -lt $EndDateNo) -and ($EndDateNo -lt 0))

    # Format
    $StartDate = $DateRange[$StartDateNo-1].Date  
    $EndDate = $DateRange[$EndDateNo-1].Date 

    # Get-MessageTrace
    try {
        # Splatting https://devblogs.microsoft.com/scripting/use-splatting-to-simplify-your-powershell-scripts/
        $TraceInfo = @{
            StartDate = $StartDate
            EndDate = $EndDate
        }
        # Check if Sender/Receiver is empty, add property
        if($SenderMailbox){$Traceinfo.Add('SenderAddress', $SenderMailbox)}
        if($ReceiverMailbox){$Traceinfo.Add('RecipientAddress', $ReceiverMailbox)}
        
        Write-Information "Displaying information about message trace if available" -InformationAction Continue
        $TraceResults = Get-MessageTrace @TraceInfo | Get-MessageTraceDetail
        $TraceResults | Out-GridView -Title 'Get-MessageTrace Results'
    }
    catch {
        Write-Warning -Message $Error[0]
    }
    finally {
        Disconnect-ExchangeOnline
    }
}