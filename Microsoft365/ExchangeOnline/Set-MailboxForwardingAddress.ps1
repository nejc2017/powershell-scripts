# Set Mailbox forwarding address


if (-not(Get-Module -ListAvailable -Name ExchangeOnlineManagement)) {
    Install-Module -Name ExchangeOnlineManagement
}

try {
    Get-EXOMailbox -ErrorAction Stop | format-table
}
catch {
    Write-Warning $Error[0]
    Connect-ExchangeOnline
}

# Set forwarding address
# Show all users, allow to select one
try {
    $AllMailboxes =  Get-EXOMailbox
    Write-Information 'Select a user mailbox' -InformationAction Continue
    $SelectedMailbox = $AllMailboxes | Out-GridView -Title 'Select a user mailbox to add an autoreply' -OutputMode Single

    Write-Information 'Select a user mailbox to forward to' -InformationAction continue
    $SelectedForwardingMailbox = $AllMailboxes | Out-GridView -Title 'Select a user mailbox to forward to' -OutputMode Single
    Set-Mailbox -Identity $SelectedMailbox.UserPrincipalName -DeliverToMailboxAndForward:$True -ForwardingAddress $SelectedForwardingMailbox.UserPrincipalName
}
catch {
    Write-Warning $Error[0]
}
finally {
    Get-Mailbox -Identity $SelectedMailbox.UserPrincipalName | select UserPrincipalName, ForwardingAddress, ForwardingSMTPAddress, DeliverToMailboxAndForward | out-gridview
    Disconnect-ExchangeOnline
}