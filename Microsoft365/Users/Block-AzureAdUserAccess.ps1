# Block access to a specific user based on his UPN
###

# Is the module installed
if (-not(Get-Module -ListAvailable -Name AzureAD)) {
    Install-Module -Name AzureAD -Scope CurrentUser
}

# check AzureAd connection
try {
    Get-AzureADUser
}
catch {
    Write-Warning $Error[0]
    Connect-AzureAD
}

try {
    $SelectedUsers = Get-AzureADUser | select Objectid, DisplayName, UserPrincipalName, AccountEnabled | Out-Gridview -Title 'Please enter the UPN of the user which should have blocked access' -PassThru
    
    foreach ($User in $SelectedUsers) {
        Set-AzureADUser -ObjectID $User.ObjectId -AccountEnabled $false
    }
}
catch {
    Write-Warning $Error[0]
}

# Display status
$DisplayUsers = New-Object System.Collections.Generic.List[PSCustomObject]

foreach ($User in $SelectedUsers) {
    $DisplayUsers.Add($(Get-AzureADUser -ObjectID $User.ObjectId))
}
$DisplayUsers | Select UserPrincipalName, AccountEnabled | Out-Gridview -Title 'Newly disabled users'