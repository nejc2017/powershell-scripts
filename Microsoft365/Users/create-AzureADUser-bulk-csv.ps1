# Create an AzureAD user: ask if single user or import from CSV
# Url: https://docs.microsoft.com/en-us/powershell/azure/active-directory/new-user-sample?view=azureadps-2.0
# Book: Widl: MS Office 365, 2020, str. 197
# # #

using namespace Microsoft.Open.AzureAD.Model

function CreateUser {
    param (
        [PSCustomObject]$User
    )

    try {
        $PasswordProfile = New-Object Microsoft.Open.AzureAD.Model.PasswordProfile
        $PasswordProfile.Password = $User.Password

        # The user will have to change the provided password at next logon
        $PasswordProfile.ForceChangePasswordNextLogin = $True

        New-AzureADUser -DisplayName $User.DisplayName -MailNickName $User.MailNickName -UserPrincipalName $User.UPN -AccountEnabled $User.AccountEnabled -PasswordProfile $PasswordProfile
        Write-Information -MessageData ('User ' + $User.DisplayName + ' was created') -InformationAction Continue
    }
    catch {
        Write-Warning $Error[0]
        break
    }
}

# Validate the string text input, if there is an error ask the user for input
# https://stackoverflow.com/questions/13738634/how-can-i-check-if-a-string-is-null-or-empty-in-powershell
function ValidateInput {
    param (
        [String]$TextInput
    )
    $IsValidated = $False
    if ([string]::IsNullOrWhitespace($TextInput)) {
        do {
            $TextInput = Read-Host -Prompt "Please enter the user's detail"
            ValidateInput $TextInput
        } while (
            $IsValidated = $False
        )
    }
    else {
        $TextInput
    }
}



# Start by connecting to AzureAD


Write-Information -MessageData "Connecting to Azure AD. Please enter your credentials in the window" -InformationAction continue
Connect-AzureAD

# Ask if a single user should be created or a CSV file imported
$SingleUserOrBulkCSV = Read-Host -Prompt 'Create a single user (1) or import CSV of multiple users (2)'


# Single user
if ($SingleUserOrBulkCSV -eq "1"   ) {

    $NameInput = ValidateInput $(Read-Host -Prompt 'Enter the users Display name')
    $MailNickNameInput = ValidateInput $(Read-Host -Prompt 'Enter the email alias - MailNickName')
    $UPNInput = ValidateInput $(Read-Host -Prompt 'Enter the User Principal Name')

    # Check Bool input
    $AccountEnabledInput = $Null
    do {
        $AccountEnabledInput = Read-Host -Prompt 'Should the Account be enabled? Y/N'
    } while (
        -Not(($AccountEnabledInput.ToLower() -eq 'y') -or ($AccountEnabledInput.ToLower() -eq 'n'))
    )
    if ($AccountEnabledInput.ToLower() -eq 'y') {
        $AccountEnabledInput = $True
    }
    elseif ($AccountEnabledInput.ToLower() -eq 'n') {
        $AccountEnabledInput = $False
    }
    # $AccountEnabledBool = [System.Convert]::ToBoolean($AccountEnabledInput)
    $PasswordInput = ValidateInput $(Read-Host -Prompt 'Enter the users password')

    # Single user object
    $NewUser = [PSCustomObject]@{
        DisplayName = $NameInput
        MailNickName = $MailNickNameInput
        UPN = $UPNInput
        AccountEnabled = $AccountEnabledInput
        Password = $PasswordInput
    }

    CreateUser $NewUser
}

# Bulk CSV import
elseif ($SingleUserOrBulkCSV -eq "2") {

    try {
        # TODO change to _>Read-Host -Prompt "Please enter the location of the users CSV file"
        # $CSVPath = "C:\Users\admin\OneDrive\Documents\Visual Studio 2017\Projects\powershell-scripts\Microsoft365\test-bulk-user-import.csv"
        do {
            $CSVPath = Read-Host -Prompt 'Please enter the location of the users CSV file'
        } until (
            Test-Path -Path $CSVPath -IsValid
        )
        $CSVImport = Import-Csv -Path $CSVPath -Encoding UTF8 -Delimiter ',' -ErrorAction Stop
        Write-Information -MessageData ('Succesfully imported CSV file. Number of entries: ' + $CSVImport.Count) -InformationAction Continue
    }
    catch {
        Write-Warning $Error[0]
        break
    }

    # Create new user objects, validate input, and add them to the list
    # https://charbelnemnom.com/how-to-create-bulk-users-in-azure-active-directory-with-powershell-azuread-powershell-aad/

    foreach ($CSVRow in $CSVImport) {
        # Check if a cell in the row is empty
        Write-Information -MessageData ('Validating user: ' + $CSVRow."Display Name" +', UPN: ' + $CSVRow."User Principal Name") -InformationAction Continue
        if (-Not($CSVRow."Display Name")) {
            Write-Warning 'Display Name is not included. Continuing with the next user entry.'
            Continue
        }
        if (-Not($CSVRow."Mail Nickname")) {
             Write-Warning 'Mail Nickname is not included. Continuing with the next user entry.'
             Continue
        }
         if (-Not($CSVRow."User Principal Name")) {
            Write-Warning 'User Principal Name is not included. Continuing with the next user entry.'
            Continue
        }

        if (-Not($CSVRow."Account Enabled")) {
            Write-Warning 'Account Enabled is not included. Continuing with the next user entry.'
            Continue
        }
        elseif (-Not(
            ($CSVRow."Account Enabled".ToLower() -eq "$true") -Or ($CSVRow."Account Enabled".ToLower() -eq "$false" )
        )) {
            Write-Warning 'Account Enabled: incorrect value: $CSVRow."Account Enabled". Continuing with the next user entry.'
            Continue
        }
        if (-Not($CSVRow.Password)) {
            Write-Warning 'Password is not included. Continuing with the next user entry.'
            Continue
        }
        if (-Not($CSVRow.Licenses)) {
            Write-Warning 'Licenses is not included. Continuing with the next user entry.'
            Continue
        }
    

        # Create the user in azuread
        try {

            $PasswordProfile = New-Object PasswordProfile
            $PasswordProfile.Password = $CSVRow.Password
            # The user will have to change the provided password at next logon
            $PasswordProfile.ForceChangePasswordNextLogin = $True
            $BoolAccountEnabled = [System.Convert]::ToBoolean($CSVRow.AccountEnabled)
            New-AzureADUser -DisplayName $CSVRow."Display Name" -MailNickName $CSVRow."Mail Nickname" -UserPrincipalName $CSVRow."User Principal Name" -AccountEnabled $BoolAccountEnabled -PasswordProfile $PasswordProfile
            Write-Information -MessageData ('User ' + $CSVRow."Display Name" + ' was created') -InformationAction Continue
        }
        catch {
            Write-Warning $Error[0]
            break
       }
    }
}
else {
    Write-Information "Please select single user (1) or CSV bulk import of multiple users (2). Exiting ..." -InformationAction Continue
}



