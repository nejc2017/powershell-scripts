# Connect to M365 tenant

# check if script is running with administrator rights and run it as admin
#requires -version 4.0
#requires -RunAsAdministrator
Write-Information "PowerShell is run as administrator" -InformationAction Continue

if (-not(Get-Module -ListAvailable -Name AzureAD)) {
    Install-Module -Name AzureAD -Scope CurrentUser
}
Import-Module AzureAD

# tenantID for Lotric unLTD test tenant
$TenantID = 'd22b8946-bc06-4ba3-b774-3c9fecf2f5dc'
Connect-AzureAD -TenantId $TenantID -AccountId 'nejc@xn--lotri-lya.com'
