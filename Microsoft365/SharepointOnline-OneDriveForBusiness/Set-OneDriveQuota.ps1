# Set OneDrive storage quota for a specific user

# Confirm the module is installed
if (Get-Module -ListAvailable -Name Microsoft.Online.SharePoint.PowerShell) {
    Write-Information 'Module Microsoft.Online.SharePoint.PowerShell exists' -InformationAction Continue 
} 
else {
    Write-Information -Message "Module does not exist. Installing Microsoft.Online.SharePoint.PowerShell for current user" -InformationAction Continue
    Install-Module Microsoft.Online.SharePoint.PowerShell -Scope CurrentUser
}

# Connect to Sharepoint
try {
    Get-SpoSite -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    $TenantDomain = Read-Host "Connecting to Sharepoint Service. Please enter your tenant domainname"
    Write-Information "Connecting to $AdminSiteUrl. Please enter your credentials." -InformationAction continue
    $AdminSiteUrl = "https://$TenantDomain-admin.sharepoint.com"
    Connect-SPOService -Url $AdminSiteUrl
}

$AllSpoSites = Get-Sposite -IncludePersonalSite $True | Where-Object {$_.Url -like "*/personal/*"}
$SelectedSpoSite = $AllSpoSites | Out-GridView -Title "Please select a OneDrive to change storage quota" -OutputMode Single


try {
    [int]$NewStorageQuota =Read-Host "Please enter the new storage quota in GB"
    # Convert to MB
    [int]$NewStorageQuotaMB = $NewStorageQuota * 1024
    Write-Information 'Setting new storage quota.' -InformationAction continue
    Set-SpoSite -Identity $SelectedSpoSite.Url -StorageQuota $NewStorageQuotaMB
}
catch {
    Write-Warning $Error[0]
}
finally {
    Write-Information 'Displaying new storage quota. Please check the MS OneDrive storage quota limits' -InformationAction continue
    Get-SpoSite -Identity $SelectedSpoSite.Url | out-gridview
}