# Get all Sharepoint site collections
###

if (-not(Get-Module -ListAvailable -Name Microsoft.Online.Sharepoint.Powershell)) {
    Install-Module -Name Microsoft.Online.Sharepoint.Powershell
}
Import-Module Microsoft.Online.Sharepoint.Powershell

# Connect to Sharepoint
try {
    Get-SpoSite -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    $TenantDomain = Read-Host "Connecting to Sharepoint Service. Please enter your tenant domain name"
    Write-Information "Connecting to $AdminSiteUrl. Please enter your credentials." -InformationAction continue
    $AdminSiteUrl = "https://$TenantDomain-admin.sharepoint.com"
    Connect-SPOService -Url $AdminSiteUrl
}


# Get a list of all sites
Write-Information 'Opening Sharepoint sites report in Out-Gridview' -InformationAction Continue
Get-SPOSite | Select *| Out-Gridview -Title 'Sharepoint Sites report'


# CSV export
$UserInput = Read-Host -Prompt 'Export to CSV? Y/N'
if ($UserInput -eq 'y') {

    # Issues with Get-Location when debugging in VS Code / ISE
    # Solution: https://stackoverflow.com/a/62604759
    $Directorypath = if ($PSScriptRoot) { $PSScriptRoot } `
    elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
    elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }


    $TimeStamp = Get-Date -Format "yyyy-MM-dd-ffff"
    $OneDriveSpoSites | Sort-Object StorageUsageCurrent | Export-CSV -Path "$Directorypath\$TimeStamp-SharepointSites-CSVExport.csv" -Encoding UTF8 -Delimiter ';'
}
elseif ($UserInput.ToLower() -eq 'n'){
    Write-Information 'Selected No. Exiting ...' -InformationAction continue
}