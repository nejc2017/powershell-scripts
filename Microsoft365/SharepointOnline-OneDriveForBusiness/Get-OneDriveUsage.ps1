# Get Storage Usage for all Onedrive accounts
# https://office365itpros.com/2019/10/10/report-onedrive-business-storage/
# Works with Powershell 5.1, throws error with PS 7.2.3
###

# Confirm the module is installed
if (Get-Module -ListAvailable -Name Microsoft.Online.SharePoint.PowerShell) {
    Write-Information 'Module Microsoft.Online.SharePoint.PowerShell exists' -InformationAction Continue 
} 
else {
    Write-Information -Message "Module does not exist. Installing Microsoft.Online.SharePoint.PowerShell for current user" -InformationAction Continue
    Install-Module Microsoft.Online.SharePoint.PowerShell -Scope CurrentUser
}

# Connect to Sharepoint
try {
    Get-SpoSite -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    $TenantDomain = Read-Host "Connecting to Sharepoint Service. Please enter your tenant domainname"
    Write-Information "Connecting to $AdminSiteUrl. Please enter your credentials." -InformationAction continue
    $AdminSiteUrl = "https://$TenantDomain-admin.sharepoint.com"
    Connect-SPOService -Url $AdminSiteUrl
}



try {
    $OneDriveSpoSites = Get-SPOSite -IncludePersonalSite $True -Limit All -Filter "Url -like '-my.sharepoint.com/personal/'" | Select-Object Title, Owner, Status, StorageUsageCurrent, StorageQuota, StorageQuotaWarningLevel, StorageQuotaType, ResourceQuota, ResourceQuotaWarningLevel, LastContentModifiedDate, SiteDefinedSharingCapability, Url
    }
catch {
    Write-Warning $Error[0]
    break
}

# Export to Out-Gridview
$OneDriveSpoSites | Sort-Object StorageUsageCurrent |  Out-GridView -Title "OneDrive report for $AdminSiteUrl"

# CSV export
do {
    $UserInput = Read-Host -Prompt 'Export to CSV? Y/N'
} while (
    -Not(($UserInput.ToLower() -eq 'y')) -or ($UserInput.ToLower() -eq 'n')
)
if ($UserInput -eq 'y') {

    # Issues with Get-Location when debugging in VS Code / ISE
    # Solution: https://stackoverflow.com/a/62604759
    $Directorypath = if ($PSScriptRoot) { $PSScriptRoot } `
    elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
    elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }


    $TimeStamp = Get-Date -Format "yyyy-MM-dd-ffff"
    $OneDriveSpoSites | Sort-Object StorageUsageCurrent | Export-CSV -Path "$Directorypath\$TimeStamp-OneDrive-CSVExport.csv" -Encoding UTF8 -Delimiter ';'
}