# Get list of files/folders from a Sharepoint site
# using PnP Powershell
###


# check if the PnP module is installed
if (-not(Get-Module -ListAvailable -Name PnP.PowerShell)) {
    Install-Module -Name PnP.PowerShell -Scope CurrentUser
}
Import-Module PnP.PowerShell

# Check if session is already connected

#Sharepoint SPO session
$SharepointSiteUrl = $Null
try {
    Get-SpoSite -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    $TenantDomain = Read-Host "Connecting to Sharepoint Service. Please enter your tenant domain name"
    Write-Information "Connecting to $AdminSiteUrl. Please enter your credentials." -InformationAction continue
    $AdminSiteUrl = "https://$TenantDomain-admin.sharepoint.com"
    Connect-SPOService -Url $AdminSiteUrl

    Write-Information 'Sharepoint Site: Files/Folder report.' -InformationAction continue
    Write-Information 'Showing all Sharepoint sites in Out-GridView, please select one.' -InformationAction continue
    $SharepointSiteUrl = Get-SPOSite | Select Title, Url, Template | Out-Gridview -Title 'Sharepoint Sites. Please select one' -OutputMode Single
}

# PNP
try {
    # Check if the session is active by getting the current request 
    Get-PnpSite
}
catch {
    Write-Warning -Message $Error[0]
    # use "interactive" for MFA accounts
    Connect-PnPOnline -Url $SharepointSiteUrl.Url -Interactive
}



# List files in Sharepoint Site
# Source: https://www.sharepointdiary.com/2018/08/sharepoint-online-powershell-to-get-all-files-in-document-library.html
try {

    # Offer the user to select the list type
    $ListName= Get-PnpList | Out-GridView -Title 'Please select a list type' -OutputMode Single

    # Get All Files/Folders from the document library - In batches of 500
    $ListItems = Get-PnPListItem -List $ListName.Title -PageSize 500 | Where {$_["FileLeafRef"] -like "*"}
    
    #Loop through all documents
    $DocumentsData=@()
    ForEach($Item in $ListItems)
    {
        #Collect Documents Data
        $DocumentsData += New-Object PSObject -Property @{
        FileName = $Item.FieldValues['FileLeafRef']
        FileURL = $Item.FieldValues['FileRef']
        }
    }

    # Sharepoint online get all files in document library powershell
    Write-Information 'Opening the Out-Gridview with a report of files/folders' -InformationAction Continue
    $DocumentsData | Out-GridView -Title 'Sharepoint files report'

    # CSV export
    $UserInput = Read-Host -Prompt 'Export to CSV? Y/N'
    if ($UserInput.ToLower() -eq 'y') {

        # Issues with Get-Location when debugging in VS Code / ISE
        # Solution: https://stackoverflow.com/a/62604759
        $Directorypath = if ($PSScriptRoot) { $PSScriptRoot } `
        elseif ($psise) { split-path $psise.CurrentFile.FullPath } `
        elseif ($psEditor) { split-path $psEditor.GetEditorContext().CurrentFile.Path }

        
        $TimeStamp = Get-Date -Format "yyyy-MM-dd-ffff"
        $OneDriveSpoSites  | Export-CSV -Path "$Directorypath\$TimeStamp-SharepointFiles-CSVExport.csv" -Encoding UTF8 -Delimiter ';'
        Write-Information "Exporting to $Directorypath\$TimeStamp-SharepointFiles-CSVExport.csv" -InformationAction continue
    }
}
catch {
    Write-Warning $Error[0]
}
finally {
    Disconnect-Sposervice
    Disconnect-PnPOnline
    Write-Information 'Disconnecting PNP Session and Spo service...' -InformationAction continue
}
