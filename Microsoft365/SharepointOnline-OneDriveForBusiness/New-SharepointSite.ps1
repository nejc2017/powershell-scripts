# Create a new Sharepoint Site


if (-not(Get-Module -ListAvailable -Name Microsoft.Online.Sharepoint.Powershell)) {
    Install-Module -Name Microsoft.Online.Sharepoint.Powershell
}
Import-Module Microsoft.Online.Sharepoint.Powershell

# Connect to Sharepoint
try {
    Get-SpoSite -ErrorAction Stop
}
catch {
    Write-Warning $Error[0]
    $TenantDomain = Read-Host "Connecting to Sharepoint Service. Please enter your tenant domain name"
    Write-Information "Connecting to $AdminSiteUrl. Please enter your credentials." -InformationAction continue
    $AdminSiteUrl = "https://$TenantDomain-admin.sharepoint.com"
    Connect-SPOService -Url $AdminSiteUrl
}

$Url=""
$Owner=""
$StorageQuota=""
$CompatibilityLevel=""
$LocaleID=""
$ResourceQuota=""
$Template=""
$TimeZoneID=""
$Title=""

# New-SPOSite -Url https://contoso.sharepoint.com/sites/mynewsite -Owner joe.healy@contoso.com -StorageQuota 1000 -CompatibilityLevel 15 -LocaleID 1033 -ResourceQuota 300 -Template "STS#0" -TimeZoneId 13 -Title "My new site collection"
New-SPOSite -Url $Url -Owner $Owner -StorageQuota $StorageQuota -CompatibilityLevel $CompatibilityLevel -LocaleID $LocaleID -ResourceQuota $ResourceQuota -Template $Template -TimeZoneId $TimeZoneID -Title $Title