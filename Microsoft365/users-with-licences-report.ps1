# Connect to M365 tenant

# # # check if script is running with administrator rights and run it as admin
# # #requires -version 4.0
# # #requires -RunAsAdministrator
# # Write-Information "PowerShell is running as administrator" -InformationAction Continue

# # if (-not(Get-Module -ListAvailable -Name AzureAD)) {
# #     Install-Module -Name AzureAD
# # }
# Import-Module AzureAD

# tenantID for Lotric unLTD test tenant
$TenantID = 'd22b8946-bc06-4ba3-b774-3c9fecf2f5dc'
Connect-AzureAD -TenantId $TenantID -AccountId 'nejc@xn--lotri-lya.com'
Get-AzureADTenantDetail | select * | out-gridview


<#
    Tenant report:
    show info about domains, provision plans, ... supported services
#> 
$ADVerifiedDomains = (Get-AzureADTenantDetail).VerifiedDomains 
$ADProvisionedPlans = (Get-AzureADTenantDetail).ProvisionedPlans
$ADUsers = Get-AzureADUser
$ADGroups = Get-AzureADMSGroup

<#
    Licences report:
    capability, applied, available, suspended, service plan
#>


<#
    Users report:
    applied licences, service plans, member of groups
#>

# a user object with added licences and service packs
$ADUserAssignedLicence = [pscustomobject]@{
    'ObjectId' = $Null
    'DisplayName' = $Null
    'UserPrincipalName' = $Null
    'UserType' = $Null
    'AssignedLicences' = $Null
    'SkuId' = $Null
    'ServicePlanId' = $Null
    'ServicePlanIncluded' = $Null
    'ServicePlanProvisStatus' = $Null
    'ServicePlanFriendlyname' = $Null

}

# a collection of users with assigned licences and service packs
$ADUserAssignedLicenceRows = New-Object System.Collections.Generic.List[PSCustomObject]

foreach ($User in $ADUsers) {
    Get-MgUserLicenseDetail -UserId $User.UserPrincipalName
}

# copied from  https://docs.microsoft.com/en-us/microsoft-365/enterprise/view-account-license-and-service-details-with-microsoft-365-powershell?view=o365-worldwide

$userUPN="nejc@xn--lotri-lya.com"
$allLicenses = Get-MgUserLicenseDetail -UserId $userUPN -Property SkuId, SkuPartNumber, ServicePlans
# $allLicenses | ForEach-Object {

# }

# $allLicenses | ForEach-Object {
#     # Write-Host "License:" $_.SkuPartNumber
#     # $_.ServicePlans 
#     # | ForEach-Object {$_}
# }
foreach ($Lic in $allLicenses) {
    # create a new user with a service plan (new row) 

    $ADNewUserWithLic = [pscustomobject]@{
        'ObjectId' = $Null
        'DisplayName' = $Null
        'UserPrincipalName' = $Null
        'UserType' = $Null
        'AssignedLicences' = $Null
        'SkuId' = $Null
        'ServicePlanId' = $Null
        'ServicePlanIncluded' = $Null
        'ServicePlanProvisStatus' = $Null
        'ServicePlanFriendlyname' = $Null
    
    }
    # assign values from the licence
    $ADNewUserWithLic.AssignedLicences = $Lic.SkuPartNumber
    $ADNewUserWithLic.SkuId = $Lic.SkuId

    
    # check service plans and assing them to the user
    foreach ($servPlan in $Lic.ServicePlans) {
        $ADNewUserWithLic.ServicePlanProvisStatus = $servPlan.ProvisioningStatus
        $ADNewUserWithLic.ServicePlanId = $servPlan.ServicePlanId
        $ADNewUserWithLic.ServicePlanIncluded = $servPlan.ServicePlanName

    } 
    
    $ADUserAssignedLicenceRows.Add($ADNewUserWithLic)
}


$ADUserAssignedLicenceRows | Out-GridView


$ScriptImportLicServicePlans = $PWD.ToString() +"\Microsoft365\import-licences-serviceplans.ps1"
.$ScriptImportLicServicePlans
$CSVLicencesServicePlans = Get-LicencesServicesPlans
$CSVLicencesServicePlans |out-gridview

