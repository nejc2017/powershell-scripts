<#
Download the "Product names and service plan identifiers for licensing" CSV and import it
Url: https://docs.microsoft.com/en-us/azure/active-directory/enterprise-users/licensing-service-plan-reference
### #>

$Url = 'https://download.microsoft.com/download/e/3/e/e3e9faf2-f28b-490a-9ada-c6089a1fc5b0/Product%20names%20and%20service%20plan%20identifiers%20for%20licensing.csv'
$FilePath = 'Product%20names%20and%20service%20plan%20identifiers%20for%20licensing.csv'

function Get-LicencesServicesPlans {

    #check if file exists, confirm to ovewrite or continue with the old file
    if (Test-Path -Path $FilePath -PathType Leaf){
        $Confirmation = Read-Host -Prompt 'CSV already downloaded. Overwrite? Y/N'
        if ($Confirmation.ToUpper() -eq 'Y') {
            try {
                Invoke-WebRequest -Uri $Url -OutFile $FilePath
                Write-Information 'CSV file downloaded' -InformationAction Continue
            }
            catch {
                Write-Information -Message ('Exception occured: ' + $_.Exception.Message) -InformationAction Continue
            }
        }
        else {
            Write-Information -Message 'Continuing with the previously saved CSV' -InformationAction Continue
            $ImportedLicencesPlans = Import-Csv -Delimiter ',' -Encoding UTF8 -Path $FilePath
            # $ImportedLicencesPlans | Out-GridView
            return $ImportedLicencesPlans
        }
    }
    # if there is no CSV saved on the disk
    else{

        try {
            Invoke-WebRequest -Uri $Url -OutFile $FilePath
            Write-Information 'CSV file downloaded' -InformationAction Continue
        }
        catch {
            Write-Information -Message ('Exception occured: ' + $_.Exception.Message) -InformationAction Continue
        }
            Write-Information -MessageData 'Importing new CSV file' -InformationAction continue
            $ImportedLicencesPlans = Import-Csv -Delimiter ',' -Encoding UTF8 -Path $FilePath
            return $ImportedLicencesPlans
    }
}

function Get-NewCSV {
    try {
        Invoke-WebRequest -Uri $Url -OutFile $FilePath
        Write-Information 'New CSV file downloaded' -InformationAction Continue
    }
    catch {
        Write-Information -Message ('Exception occured: ' + $_.Exception.Message) -InformationAction Continue
    }
    
}
# Run function and ask if it should be show in a windows
$SavedLicences = Get-LicencesServicesPlans 

$ConfirmationOpen = Read-Host -Prompt 'Open Licences and Service Packs in Out-Gridview? Y/N'
if ($ConfirmationOpen.ToUpper() -eq 'Y') {
   $SavedLicences | Out-Gridview -Title 'Licences and Service Plans'
}
