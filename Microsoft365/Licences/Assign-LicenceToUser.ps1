# Assign a licence to a specific user
# https://docs.microsoft.com/en-us/microsoft-365/enterprise/assign-licenses-to-user-accounts-with-microsoft-365-powershell?view=o365-worldwide
###

# Microsoft Graph
if (-not(Get-Module -ListAvailable -Name 'Microsoft.Graph')) {
    Install-Module Microsoft.Graph -Scope CurrentUser
}
# Connect to MS Graph with ReadWrite.All and Read.All permissions
try {
    Get-MgUser -ErrorAction Stop | format-table
    Write-Information 'MS Graph is connected. Displaying Users' -InformationAction Continue

}
catch {
    Write-Warning $Error[0]
    Write-Information 'MS Graph not connected. Connecting, if required please enter your credentials' -InformationAction Continue
    Connect-MgGraph -Scopes User.ReadWrite.All, Organization.Read.All
}

# AzureAD to correctly display usage location
if (-not(Get-Module -ListAvailable -Name AzureAD)) {
    Install-Module -Name AzureAD -Scope CurrentUser
}
# check AzureAd connection
try {
    Get-AzureADUser -ErrorAction Stop | format-table
}
catch {
    Write-Warning $Error[0]
    Write-Information 'Please enter your credentials if required' -InformationAction Continue
    Connect-AzureAD
}


function Assign-Licence {
    [CmdletBinding()]
    param (
        [Parameter()]
         $ProvidedUser 
    )

    # Select licences
    # Get all licences and display which are available
    $SkuSubscribed = Get-MgSubscribedSku | Select -Property Sku*, ConsumedUnits -ExpandProperty PrepaidUnits | select * 
    # Using Out-GridView with the -PassThru parameter -> UI to select licences
    $SelectedSku = $SkuSubscribed | Out-GridView -Title 'Please select a SKU to assign and click OK' -PassThru

    # Usage location must be set before assigning a licence. Check if usage location is set
    # Since Get-MgUser doesnt correctly show the Usage location field, Get-AzureAdUser is called
    $SelectedUser = Get-AzureADUser -ObjectId $ProvidedUser.UserPrincipalName
    if ($SelectedUser.UsageLocation -eq $Null) {
        $InputLocation = Read-Host -Prompt 'Please enter the usage location with a valid ISO 3166-1 alpha-2 country code'
        
        try {
            Update-MgUser -UserId $SelectedUser.UserPrincipalName -UsageLocation $InputLocation -ErrorAction Stop
            Write-Information "Usage was set to $InputLocation" -InformationAction Continue
        }
        catch {
            Write-Information 'Usage location not set' -InformationAction Continue
            Write-Warning $Error[0]
        }
    }

    # Set the licence
    try {
        $SkuId = $SelectedSku.SkuId
        Set-MgUserLicense -UserId $ProvidedUser.UserPrincipalName -AddLicenses @{SkuId = $SkuId } -RemoveLicenses @()
    }
    catch {
        Write-Warning $Error[0]
    }
  }

# Start by showing either all users or only unlicensed account
$UserWithAssignedLicence = $Null
$InputUserAccount = Read-Host -Prompt 'Show unlicensed user accounts? Y/N'
if ($InputUserAccount.ToLower() -eq 'y') {
    $UnlicensedUser = Get-MgUser -Filter 'assignedLicenses/$count eq 0' -ConsistencyLevel eventual -CountVariable unlicensedUserCount -All
    $UserWithAssignedLicence = $UnlicensedUser | Out-GridView -PassThru -Title 'Please select the users which should get a licence assigned'
        
    foreach ($CurrentUser in $UserWithAssignedLicence) {
        Assign-Licence -ProvidedUser $CurrentUser
    }
}
else {
    Write-Information 'Showing all user accounts' -InformationAction Continue
    $AllUsers = Get-Mguser  
    $UserWithAssignedLicence = $AllUsers | Out-Gridview -Title 'Please select a user and click OK' -PassThru
    
    foreach ($CurrentUser in $UserWithAssignedLicence) {
        Assign-Licence -ProvidedUser $CurrentUser
    }
    
}


# Confirm licence is assigned
try {
    $DisplayUsers = New-Object System.Collections.Generic.List[PSCustomObject]

    foreach ($User in $UserWithAssignedLicence) {
        $UserInfo = Get-MgUserLicenseDetail -UserId $User.UserPrincipalName
        $UserInfo | Add-Member -Name 'UPN' -MemberType NoteProperty -Value $User.UserPrincipalName
        $DisplayUsers.Add($UserInfo)
    }
    Write-Information 'Displaying users with newly assigned licences' -InformationAction Continue
    $DisplayUsers | select * | Out-GridView -Title "Users Overview"
}
catch {
    Write-Warning $Error[0]
}