<#
    Get info about the licences and service plans assigned to the users
# # # #>

if (-not(Get-Module -Name 'Microsoft.Graph')) {
    Install-Module Microsoft.Graph -Scope CurrentUser
}

try {
    Get-MgSubscribedSku -ErrorAction Stop
}
catch {
    Write-Information 'MS Graph not connected. Connecting, please enter your credentials'
    Connect-MgGraph -Scopes User.ReadWrite.All, Organization.Read.All
}

try {
    $DisplayUsers = New-Object System.Collections.Generic.List[PSCustomObject]
    $AllUsers = Get-Mguser
    $UserCount = 0

    foreach ($User in $AllUsers) {
        $UserCount++
        $User | Add-Member -Name 'UPN' -MemberType NoteProperty -Value $User.UserPrincipalName
        $User | Add-Member -Name 'UserCount' -MemberType NoteProperty -Value $UserCount
        # License Details
        $UserDetails = Get-MgUserLicenseDetail -UserId $User.UserPrincipalName
        $User | Add-Member -Name 'SKUID' -MemberType NoteProperty -Value $UserDetails.SkuId
        $User | Add-Member -Name 'SKUPartNumber' -MemberType NoteProperty -Value $UserDetails.SkuPartNumber
        $User | Add-Member -Name 'ServicePlans' -MemberType NoteProperty -Value $UserDetails.ServicePlans
        $DisplayUsers.Add($User)
    }

    Write-Information 'Displaying users and licences' -InformationAction Continue
    $DisplayUsers | select UserCount, Id, DisplayName, UPN, SKUID, SkuPartNumber, ServicePlans | Out-GridView -Title "Users Overview"
}
catch {
    Write-Warning $Error[0]
}



